Rails.application.routes.draw do
 root 'home#index'
 get '/watch' => 'home#index'
 get '/searched' => 'home#search'
 get '/privacy' => 'home#privacy'
end
