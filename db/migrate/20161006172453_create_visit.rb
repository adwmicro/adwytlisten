class CreateVisit < ActiveRecord::Migration[5.0]
  def change
    create_table :visits do |t|
      t.integer    :visit_count,             null: false, default: 0
      t.timestamps                           null: false
    end
  end
end
