class CreateMusic < ActiveRecord::Migration[5.0]
  def change
    create_table :musics do |t|
      t.string 	   :file_title,              	null: false, default: ""
      t.string 	   :youtube_id,              	null: false, default: ""
      t.integer    :played_count,             null: false, default: 0
      t.string     :author_url,               null: false, default: ""
      t.string     :author_name,               null: false, default: ""
      t.string     :thumbnail_url,            null: false, default: ""
      t.timestamps null: false
    end
  end
end
