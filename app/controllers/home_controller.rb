class HomeController < ApplicationController
  require 'cgi'
  require 'httparty'

  def index
    if params[:v].blank?
      @get = false
    else
      @get = true
      @request_yt_id = params[:v]
      @data = get_hash_data_from_youtube(@request_yt_id)
      @music = Music.where(youtube_id: @request_yt_id).first
      if @music.blank?
         @music = download_music_to_server(@request_yt_id)
        if !@music.save()
          render json: {error: 'Can\'t find video.'}
        end
      else
        #tutaj będzie dodawanie wyświetleń
      end
    end
    render template: 'home/index'
  end

  def search
    if params[:v].blank?
      @get = false
      render template: 'home/index'
    else
      @get = true
      @request_yt_id = params[:v]
      search = Search.where(youtube_id: @request_yt_id).first
      if search.blank?
        data = get_hash_data_from_youtube(@request_yt_id)
        Search.new(
            file_title: data["title"],
            youtube_id: @request_yt_id,
            searched_count: 0,
            author_url: data["author_url"],
            author_name: data["author_name"],
            thumbnail_url: data["thumbnail_url"]
          ).save
      else
        search = Search.find_by(youtube_id: @request_yt_id)
        search.searched_count += 1
        search.save
      end
      redirect_to "https://www.youtubeplaysound.com/watch?v=#{@request_yt_id}"
    end
  end

  private

  def download_music_to_server(id)
    system("youtube-dl --no-check-certificate --verbose
    --proxy \"89.46.74.205:20250\"  --force-ipv4 --extract-audio
    --audio-format mp3 -o \"./public/audio/%(id)s.%(ext)s\" 
    https://www.youtube.com/watch?v=#{id}")
    data = get_hash_data_from_youtube(id)
    @to_return = Music.new(
        file_title: data["title"],
        youtube_id: id,
        played_count: 0,
        author_url: data["author_url"],
        author_name: data["author_name"],
        thumbnail_url: data["thumbnail_url"]
    )
  end

  def get_hash_data_from_youtube(id)
    HTTParty.get("https://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=#{id}&format=json", :verify => false).parsed_response.to_hash
  end

end