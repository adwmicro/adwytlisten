$(".ytl-search-result").mouseleave(function () {
    $(this).hide();
});
$("#ytl-main-search").keyup(function () {
    $(".ytl-search-result").show();
    var address = "https://www.googleapis.com/youtube/v3/search?part=snippet%20&q=" + $(this).val() + "%20&key=AIzaSyCLDVGqJTEYeVIvrD_TFM8IROLK9MqYiU8";
    $.get(address, function (e) {
        $("#ytl-sr-append").html("");
        var elements = e.items;
        $.each(elements, function (key, val) {
            var element = val.snippet;
            if (val.id.videoId != undefined) {
                var html_result = "<li class='ylr-sr-item'><a href='https://www.youtubeplaysound.com/searched?v=" + val.id.videoId + "'><div class='ytl-sr-image' style='background: url(" + element.thumbnails.default.url + ") no-repeat center center;'></div>" +
                    "<div class='ytl-sr-title'>" + element.title + "</div><div class='ytl-sr-description'>" + element.description + "</div></a></li>";
                $("#ytl-sr-append").append(html_result);
            }
        });
    });
});