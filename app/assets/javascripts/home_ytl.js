//=require jquery.flipster.min
//=require wavesurfer.min
//=require bootstrap-slider
//=require instant_search

(function ($) {
    $.fn.clickToggle = function (func1, func2) {
        var funcs = [func1, func2];
        this.data('toggleclicked', 0);
        this.click(function () {
            var data = $(this).data();
            var tc = data.toggleclicked;
            $.proxy(funcs[tc], this)();
            data.toggleclicked = (tc + 1) % 2;
        });
        return this;
    };
}(jQuery));

(function () {
    $(".ytl-main").css("min-height", $(window).height());
}());

$("#coverflow").flipster({
    style: 'flat',
    spacing: -0.25,
    loop: true,
    scrollwheel: false
});

$("#coverflow2").flipster({
    style: 'flat',
    spacing: -0.25,
    loop: true,
    scrollwheel: false
});

$(".ytl-section").hover(function () {
    $(this).find(".flipster--click").focus().click();
});

$(document).ready(function () {
    if ($("#param_check").val() != "false") {
        init_player();
    }
    $(".flipster__item").click(function () {
        playNewSong($(this).attr("yt-id"));
    });
});

function init_player() {
    var wavesurfer = WaveSurfer.create({
        container: '#waveform',
        progressColor: 'purple',
        height: 70,
        waveColor: "#ffffff",
        progressColor: "#db2b28"
    });

    $(function () {
        var file = $(".ytl-player").attr("rails-data-id");
        wavesurfer.load('/audio/' + file + '.mp3');
    }());

    $("#CP").clickToggle(function () {
            $(this).find("i").removeClass("fa-play").addClass("fa-pause").css("padding-left", "2px");
            wavesurfer.play();
            wavesurfer.setVolume(0.50);
        },
        function () {
            $(this).find("i").removeClass("fa-pause").addClass("fa-play").css("padding-left", "8px");
            wavesurfer.pause();
        });

    wavesurfer.on('ready', function () {
        $(".ytl-song-loading").fadeOut();
    });

    $("#ex4").slider({
        reversed: true
    });

    $("#ex4").on('slide', function (ev) {
        var currentVolume = parseFloat($(this).val());
        wavesurfer.setVolume(currentVolume);
    });
}

function playNewSong(id) {
    if ($("#CP").find("i").hasClass("fa-pause")) {
        $("#CP").click();
    }
    var title = $("li[yt-id='" + id + "']").find(".ytl-slider-text-title").text();
    changeDownload(id, title);
    $("#waveform").html("");
    var player = $("#ytl-player-m");
    player.find(".ytl-player").attr("rails-data-id", id);
    player.find(".ytl-player-title").text(title);
    init_player();
    player.fadeIn();
}

function changeDownload(id, title) {
    $(".ytl-l-m").find(".ytl-menu-download").remove();
    $(".ytl-l-m").find(".ytl-menu").append("<a href='https://www.youtube.com/watch?v=" + id + "'>" +
        "<div class='ytl-menu-download' type_p='true'>Youtube.com</div></a>");
}
